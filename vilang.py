#!/usr/bin/env python3

import os
import sys
import regex
import re

Keywords = [
    "cnslg",
    "vr",
    "func",
    "for"
    "end"
]

# OBJECTS
class VariableObject(object):
    def __init__(self):
        self.exec_string = ''

    def transpile(self, name, operator, value):
        self.exec_string += f'{name} {operator} {value} '
        return self.exec_string


class ConsoleLogObject(object):
    def __init__(self):
        self.exec_string = ''

    def transpile(self, value):
        self.exec_string += f'print{value} '
        return self.exec_string


# PARSER
class Parser(object):
    def __init__(self, tokens):
        self.tokens = tokens
        self.token_index = 0
        self.types = ['STRING', 'INTEGER', 'IDENTIFIER']
        self.transpiled_code = []

    def parse(self):
        while self.token_index < len(self.tokens):
            token_type = self.tokens[self.token_index][0]
            token_value = self.tokens[self.token_index][1]

            if token_type == 'IDENTIFIER' and token_value == 'vr':
                self.parse_variable_declaration(self.tokens[self.token_index:len(self.tokens)])

            if token_type == 'IDENTIFIER' and token_value == 'cnslg':
                self.parse_console_log(self.tokens[self.token_index:len(self.tokens)])

            self.token_index += 1

        for transpiled in self.transpiled_code:
            exec(transpiled)

    def parse_variable_declaration(self, token_stream):
        tokens_checked = 0

        name = ''
        operator = ''
        value = ''

        for token in range(0, len(token_stream)):
            token_type = token_stream[tokens_checked][0]
            token_value = token_stream[tokens_checked][1]

            if token == 4 and token_type == 'STATEMENT_END':
                break
            if token == 4 and token_type != 'STATEMENT_END':
                print(f'ERROR: invalid syntax; add \';\' at the end of the line')
                sys.exit(1)

            elif token == 1 and token_type == 'IDENTIFIER':
                name = token_value
            elif token == 1 and token_type != 'IDENTIFIER':
                print(f'ERROR: invalid variable name: \'{token_value}\'')
                sys.exit(1)

            elif token == 2 and token_type == 'OPERATOR':
                operator = token_value
            elif token == 2 and token_type != 'OPERATOR':
                print('ERROR: invalid assignment or missing; should be: \'=\'')
                sys.exit(1)

            elif token == 3 and token_type in self.types:
                value = token_value
            elif token == 3 and token_type not in self.types:
                print('ERROR: Invalid variable assignment: '
                      + f'\'{token_value}\'')
                sys.exit(1)

            tokens_checked += 1

        varObj = VariableObject()
        self.transpiled_code.append(varObj.transpile(name, operator, value))

        self.token_index += tokens_checked

    def parse_func(self, token_stream):
        tokens_checked = 0
        value = ''

        for token in range(0, len(token_stream)):
            token_type = token_stream[tokens_checked][0]
            token_value = token_stream[tokens_checked][1]

    def parse_console_log(self, token_stream):
        tokens_checked = 0

        value = ''

        for token in range(0, len(token_stream)):
            token_type = token_stream[tokens_checked][0]
            token_value = token_stream[tokens_checked][1]

            if token == 2 and token_type == 'STATEMENT_END': break

            if token == 1 and token_type == 'PARENTHESES':
                value = token_value

            tokens_checked += 1

        console_logObj = ConsoleLogObject()
        self.transpiled_code.append(console_logObj.transpile(value))

        self.token_index += tokens_checked


# LEXER
class Lexer(object):
    def __init__(self, source_code):
        self.source_code = source_code
        self.keywords = Keywords

    def split(self, source_code):
        reg_extract = regex.compile(r'(?:(\((?>[^()]+|(?1))*\))|\S)+')
        reg_validate = regex.compile(r'^[^()]*(\((?>[^()]+|(?1))*\)[^()]*)+$')
        if reg_validate.fullmatch(source_code):
            source_split = [x.group()
                            for x in reg_extract.finditer(source_code)]
        else:
            source_split = source_code.split()

        return source_split

    def getMatcher(self, matcher, current_index, source_code):
        if source_code[current_index].count('"') == 2:

            word = \
                source_code[current_index].partition('"')[-1].partition('"'[0])

            if word[2] != '':
                return ['"' + word[0] + '"', '', word[2]]
            else:
                return ['"' + word[0] + '"', '', '']

        else:

            source_code = source_code[current_index:len(source_code)]

            word = ""

            iter_count = 0

            for item in source_code:

                iter_count += 1

                word += item + " "

                if matcher in item and iter_count != 1:

                    return [
                        '"'
                        + word.partition('"')[-1].partition('"'[0])[0]
                        + '"',
                        word.partition('"')[-1].partition('"'[0])[2],
                        iter_count - 1]

    def tokenize(self):
        tokens = []
        source_code = self.split(self.source_code)
        source_index = 0

        while source_index < len(source_code):
            word = source_code[source_index]

            if word in "\n":
                pass

            elif word in self.keywords:
                tokens.append(["IDENTIFIER", word])

            elif re.match('[a-z]', word) or re.match('[A-Z]', word):
                if word[len(word) - 1] == ';':
                    tokens.append(['IDENTIFIER', word[0:len(word) - 1]])
                else:
                    tokens.append(['IDENTIFIER', word])

            elif re.match('[0-9]', word):
                if word[len(word) - 1] == ';':
                    tokens.append(['INTEGER', word[0:len(word) - 1]])
                else:
                    tokens.append(['INTEGER', word])

            elif re.match(r'\(([^)]+)\)', word):
                if word[len(word) - 1] == ';':
                    tokens.append(['PARENTHESES', word[0:len(word) - 1]])
                else:
                    tokens.append(['PARENTHESES', word])
            elif word in '=/*-+':
                tokens.append(['OPERATOR', word])
            elif word == '&&' or word == "||":
                tokens.append(["BINARY_OPERATOR", word])

            elif word == ';;':
                tokens.append(['SEPERATOR', word])

            elif word == '(--' or word == '--)':
                tokens.append(["COMMENT_DEFINER", word])

            elif ('"') in word:
                matcherReturn = self.getMatcher('"', source_index, source_code)
                if matcherReturn[1] == '':
                    tokens.append(["STRING", matcherReturn[0]])

                else:
                    tokens.append(["STRING", matcherReturn[0]])
                    if ';' in matcherReturn[1]:
                        tokens.append(["STATEMENT_END", ";"])
                    source_index += matcherReturn[2]

                    pass

            elif word == ';':
                tokens.append(["STATEMENT_END", ';'])
            else:
                print('ERROR: unknown word: ', word)
                sys.exit(1)

            if word[len(word) - 1] == ';':
                tokens.append(['STATEMENT_END', ';'])

            source_index += 1

        return tokens


def main():

    path = os.getcwd()

    try:
        fileName = sys.argv[2]
    except IndexError:
        print("ERROR: Expected 1 Argument Containing File Name to be Run e.g 'vilang run main.vl'")
        return

    if fileName[len(fileName) - 3:len(fileName)] != ".vl":
        print("ERROR: File extension not recognised please make sure extension is '.vl'")
        return
    try:
        print('ERROR: Expected 3 argument found 1 or 2 (' + sys.argv[2] + ", " + sys.argv[3] + ')')
        return
    except IndexError:
        pass
    if sys.argv[1] == 'run':
        try:
            with open(path + "/" + fileName, "r") as file:
                content = file.read()
        except FileNotFoundError:
            print(f'Cannot find file: {fileName}')
            quit()
    else:
        print('ERROR: argument not found')

    lex = Lexer(content)
    tokens = lex.tokenize()

    parse = Parser(tokens)
    objs = parse.parse()


if __name__ == '__main__':
    main()
